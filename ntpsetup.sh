echo "setup ntp server"
sudo yum update -y
sudo yum upgrade -y
#installing ntp server
sudo yum -y install chrony 
sed -i "s/pool 2.centos.pool.ntp.org iburst/server 0.asia.pool.ntp.org/g" /etc/chrony.conf

sudo systemctl start chronyd

sudo systemctl enable chronyd

sudo firewall-cmd --add-service=ntp --permanent 

sudo firewall-cmd --reload

#testing ntp
chronyc tracking
hwclock --systohc

